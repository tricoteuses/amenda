import { validateNonEmptyTrimmedString } from "@biryani/core"
import fetch from "cross-fetch"
import url from "url"

import serverConfig from "../../../../server_config"

const { gitlabLegislation: gitlab } = serverConfig

export async function get(req, res) {
  const { user } = req
  const accessToken = user ? user.accessToken : null
  const headers = {
    "Content-Type": "application/json; charset=utf-8",
  }
  if (accessToken !== null) {
    headers["Authorization"] = `Bearer ${accessToken}`
  }

  const [params, error] = validateParams(req.params)
  if (error !== null) {
    console.error(
      `Error in params:\n${JSON.stringify(params, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...params,
          error: {
            code: 400,
            details: error,
            message: "Invalid parameters",
          },
        },
        null,
        2
      )
    )
  }
  const { namespace: namespaceName, project: projectName } = params
  const projectPath = `${namespaceName}/${projectName}`

  // Get project.
  let project = null
  {
    const requestUrl = url.resolve(gitlab.url, `api/v4/projects/${encodeURIComponent(projectPath)}`)
    const response = await fetch(
      requestUrl,
      {
        method: "GET",
        headers,
      }
    )
    if (!response.ok) {
      const body = 400 <= response.status && response.status < 500
        ? await response.json()
        : await response.text()
      console.error(
        `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
      )
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...params,
            error: {
              body,
              code: response.status,
              message: response.statusText,
            },
            requestUrl: requestUrl,
          },
          null,
          2
        )
      )
    }
    project = await response.json()
  }

  // Get repository tree and extract informations on legislation file from it.
  let legislationNode = null
  {
    const requestUrl = url.resolve(
      gitlab.url,
      `api/v4/projects/${encodeURIComponent(projectPath)}/repository/tree`,
    )
    const response = await fetch(
      requestUrl,
      {
        method: "GET",
        headers,
      }
    )
    if (!response.ok) {
      const body = 400 <= response.status && response.status < 500
        ? await response.json()
        : await response.text()
      console.error(
        `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
      )
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...params,
            error: {
              body,
              code: response.status,
              message: response.statusText,
            },
            requestUrl: requestUrl,
          },
          null,
          2
        )
      )
    }
    const tree = await response.json()
    const markdownNodes = tree
      .filter(node => node.type === "blob" && node.name.endsWith(".md"))
    if (markdownNodes.length !== 1) {
      console.error(
        `Error while calling ${requestUrl}\n\nError: Repository tree has ${
          markdownNodes.length} markdown file(s) instead of 1`
      )
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...params,
            error: {
              code: 400,
              message: `Repository tree has ${markdownNodes.length} markdown file(s) instead of 1`,
            },
            tree,
            requestUrl: requestUrl,
          },
          null,
          2
        )
      )
    }
    legislationNode = markdownNodes[0]
  }

  // Retrieve markdown file containing legislation.
  let legislation = null
  {
    const requestUrl = url.resolve(
      gitlab.url,
      `api/v4/projects/${encodeURIComponent(projectPath)}/repository/files/${
        encodeURIComponent(legislationNode.path)}/raw?ref=${encodeURIComponent(project.default_branch)}`,
    )
    const response = await fetch(
      requestUrl,
      {
        method: "GET",
        headers,
      }
    )
    if (!response.ok) {
      const body = 400 <= response.status && response.status < 500
        ? await response.json()
        : await response.text()
      console.error(
        `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
      )
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...params,
            error: {
              body,
              code: response.status,
              message: response.statusText,
            },
            requestUrl: requestUrl,
          },
          null,
          2
        )
      )
    }
    legislation = await response.text()
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(
    {
      ...project,
      legislation,
      legislationPath: legislationNode.path,
    },
    null,
    2,
  ))
}

export async function put(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2
      )
    )
  }
  const headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Authorization": `Bearer ${user.accessToken}`,
  }

  const [params, paramsError] = validateParams(req.params)
  if (paramsError !== null) {
    console.error(
      `Error in params:\n${JSON.stringify(params, null, 2)}\n\nError:\n${JSON.stringify(paramsError, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...params,
          error: {
            code: 400,
            details: paramsError,
            message: "Invalid parameters",
          },
        },
        null,
        2
      )
    )
  }
  const { namespace: namespaceName, project: projectName } = params
  const projectPath = `${namespaceName}/${projectName}`

  const [body, bodyError] = validateBody(req.body)
  if (bodyError !== null) {
    console.error(
      `Error in form body:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(bodyError, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...params,
          ...body,
          error: {
            code: 400,
            details: bodyError,
            message: "Invalid form body",
          },
        },
        null,
        2
      )
    )
  }
  const { branch, legislation, legislationPath } = body

  // Update legislation file in project.
  let fileUpdate = null
  {
    const requestUrl = url.resolve(
      gitlab.url,
      `api/v4/projects/${encodeURIComponent(projectPath)}/repository/files/${encodeURIComponent(legislationPath)}`,
    )
    const response = await fetch(
      requestUrl,
      {
        body: JSON.stringify({
          branch,
          commit_message: "Modification",
          content: legislation ? legislation + "\n" : legislation,
        }, null, 2),
        method: "PUT",
        headers,
      }
    )
    if (!response.ok) {
      const responseBody = 400 <= response.status && response.status < 500
        ? await response.json()
        : await response.text()
      console.error(
        `Error while calling ${requestUrl}\n\nError: ${response.status} ${response.statusText}`
      )
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            ...params,
            ...body,
            error: {
              body: responseBody,
              code: response.status,
              message: response.statusText,
            },
            requestUrl: requestUrl,
          },
          null,
          2
        )
      )
    }
    fileUpdate = await response.json()
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(fileUpdate, null, 2))
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  for (let key of ["branch", "legislation", "legislationPath"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}

function validateParams(params) {
  if (params === null || params === undefined) {
    return [params, "Missing parameters"]
  }
  if (typeof params !== "object") {
    return [params, `Expected an object, got ${typeof params}`]
  }

  params = {
    ...params,
  }
  const remainingKeys = new Set(Object.keys(params))
  const errors = {}

  for (let key of ["namespace", "project"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(params[key])
    params[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [params, Object.keys(errors).length === 0 ? null : errors]
}
