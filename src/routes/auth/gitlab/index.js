import passport from "passport"

export function get(req, res, next) {
  req.session.redirect = req.query.redirect || null
  return passport.authenticate("gitlab", {
    scope: ["api"],
  })(req, res, next)
}