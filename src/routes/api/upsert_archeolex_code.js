import { validateNonEmptyTrimmedString } from "@biryani/core"
// import childProcess from "child_process"
import fetch from "cross-fetch"
// import fs from "fs-extra"
// import os from "os"
// import path from "path"
import url from "url"

import serverConfig from "../../server_config"

const { gitlabLegislation: gitlab } = serverConfig

export async function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2
      )
    )
  }

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form body:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid form body",
          },
        },
        null,
        2
      )
    )
  }

  let group = null
  const projectPath = `${gitlab.groupPath}/${body.code}`
  // let project = null
  let state = "get group"
  // let userGroup = null
  let userNamespace = null
  let userProject = null
  const userProjectPath = `${user.username}/${body.code}`
  while (state !== null) {
    switch (state) {
      case "get group":
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/groups/${encodeURIComponent(gitlab.groupPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          group = await response.json()
        }
        state = "create project"
        break

      case "create project":
        // Try to create project.
        {
          const requestUrl = url.resolve(gitlab.url, "api/v4/projects")
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                // description:
                import_url: `https://archeo-lex.fr/codes/${body.code}`,
                issues_enabled: false,
                jobs_enabled: false,
                merge_requests_enabled: false,
                // name:
                namespace_id: group.id,
                path: body.code,
                snippets_enabled: false,
                visibility: "public",
                wiki_enabled: false,
              }, null, 2),
              method: "POST",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            if (response.status === 400) {
              // Project already exists. Update it.
              state = "update project"
              break
            }
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          // project = await response.json()
        }
        state = "create user group"
        break

      case "update project":
        // TODO
        // const repositoryDir = fs.mkdtempSync(path.join(os.tmpdir(), "amenda_"))
        // console.log("repositoryDir", repositoryDir)
        // const cloneResult = childProcess.execSync(
        //   `/usr/bin/git clone https://archeo-lex.fr/codes/${body.code} ${repositoryDir}`)
        // console.log("cloneResult", cloneResult)
        // fs.removeSync(repositoryDir)
        state = "create user group"
        break

      case "create user group":
        // // Try to create an amenda group for user.
        // {
        //   const requestUrl = url.resolve(gitlab.url, "api/v4/groups")
        //   const response = await fetch(
        //     requestUrl,
        //     {
        //       body: JSON.stringify({
        //         description: "Législation amendée ou à amender (utilisée par https://amenda.tricoteuses.fr)",
        //         // name:
        //         path: gitlab.groupPath,
        //         visibility: "public",
        //       }, null, 2),
        //       method: "POST",
        //       headers: {
        //         "Authorization": `Bearer ${user.accessToken}`,
        //         "Content-Type": "application/json; charset=utf-8",
        //       },
        //     }
        //   )
        //   if (!response.ok) {
        //     const responseBody = 400 <= response.status && response.status < 500
        //       ? await response.json()
        //       : await response.text()
        //     if (response.status === 400) {
        //       // Group already exists.
        //       state = "get user group"
        //       break
        //     }
        //     console.error(
        //       `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
        //     )
        //     res.writeHead(response.status, {
        //       "Content-Type": "application/json; charset=utf-8",
        //     })
        //     return res.end(
        //       JSON.stringify(
        //         {
        //           ...body,
        //           error: {
        //             body: responseBody,
        //             code: response.status,
        //             message: response.statusText,
        //           },
        //           requestUrl: requestUrl,
        //         },
        //         null,
        //         2
        //       )
        //     )
        //   }
        //   userGroup = await response.json()
        // }
        state = "fork project"
        break

      // case "get user group":
      //   {
      //     const requestUrl = url.resolve(
      //       gitlab.url,
      //       `api/v4/groups/${encodeURIComponent(gitlab.groupPath)}`,
      //     )
      //     const response = await fetch(
      //       requestUrl,
      //       {
      //         method: "GET",
      //         headers: {
      //           "Authorization": `Bearer ${user.accessToken}`,
      //           "Content-Type": "application/json; charset=utf-8",
      //         },
      //       }
      //     )
      //     if (!response.ok) {
      //       const responseBody = 400 <= response.status && response.status < 500
      //         ? await response.json()
      //         : await response.text()
      //       console.error(
      //         `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
      //       )
      //       res.writeHead(response.status, {
      //         "Content-Type": "application/json; charset=utf-8",
      //       })
      //       return res.end(
      //         JSON.stringify(
      //           {
      //             ...body,
      //             error: {
      //               body: responseBody,
      //               code: response.status,
      //               message: response.statusText,
      //             },
      //             requestUrl: requestUrl,
      //           },
      //           null,
      //           2
      //         )
      //       )
      //     }
      //     userGroup = await response.json()
      //   }
      //   state = "fork project"
      //   break

      case "fork project":
        // Get the namespace where to fork the project.
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/namespaces/${encodeURIComponent(user.username)}`,
          )
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userNamespace = await response.json()
        }

        // Try to fork project.
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(projectPath)}/fork?namespace=${userNamespace.id}`,
          )
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                // namespace: userGroup.id,
                // namespace: user.username,
              }, null, 2),
              method: "POST",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            if (response.status === 409) {
              // Conflict: path or name have already been taken.
              // Project already exists. Update it.
              state = "update user project"
              break
            }
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
        }
        state = "wait fork finished"
        break

      case "update user project":
        // TODO: Currently, the user project is only retrieved (and not updated).
        // const repositoryDir = fs.mkdtempSync(path.join(os.tmpdir(), "amenda_"))
        // console.log("repositoryDir", repositoryDir)
        // const cloneResult = childProcess.execSync(
        //   `/usr/bin/git clone https://archeo-lex.fr/codes/${body.code} ${repositoryDir}`)
        // console.log("cloneResult", cloneResult)
        // fs.removeSync(repositoryDir)
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/projects/${encodeURIComponent(userProjectPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
        }
        state = "get legislation"
        break

      case "wait fork finished":
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/projects/${encodeURIComponent(userProjectPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
          const importStatus = userProject.import_status
          switch (importStatus) {
            case "finished":
            case "none":
              state = "get legislation"
              break
            case "scheduled":
            case "started":
              await sleep(1000)
              // Continue with same state.
              break
            default:
              console.error(
                `Error while waiting for fork completion ${requestUrl}\n\nError: Unexpected import_status: ${
                  importStatus}`
              )
              res.writeHead(response.status, {
                "Content-Type": "application/json; charset=utf-8",
              })
              return res.end(
                JSON.stringify(
                  {
                    ...body,
                    error: {
                      code: 400,
                      message: `Unexpected import_status: ${importStatus}`,
                    },
                    requestUrl: requestUrl,
                    userProject,
                  },
                  null,
                  2
                )
              )
          }
        }
        break

      case "get legislation":
        // Get repository tree
        let tree = null
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(userProjectPath)}/repository/tree`,
          )
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          tree = await response.json()
          const markdownNodes = tree
            .filter(node => node.type === "blob" && node.name.endsWith(".md"))
          if (markdownNodes.length !== 1) {
            console.error(
              `Error while calling ${requestUrl}\n\nError: Repository tree has ${
                markdownNodes.length} markdown file(s) instead of 1`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    code: 400,
                    message: `Repository tree has ${markdownNodes.length} markdown file(s) instead of 1`,
                  },
                  tree,
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
        }
        state = null
        break

      default:
        throw Error(`Unexpected state: ${state}`)
    }
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(userProject, null, 2))
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  {
    const key = "code"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
