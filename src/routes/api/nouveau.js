import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core"

export async function post(req, res) {
  const [body, error] = validateBody(req.body)
  if (error === null) {
    req.session.nouveau = body
  } else {
    console.error(
      `Error in form:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    req.session.nouveau = {
      ...body,
      error,
    }
  }
  res.writeHead(302, {
    "Location": req.baseUrl + "/nouveau",
  })
  return res.end()
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  for (let key of ["hostname", "legislation", "originalHostname", "path", "title", "type"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["originalUrl", "url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
