import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core"
import fs from "fs"
import path from "path"

function validateGitLabAuthentication(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["callbackUrl", "url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["clientId", "clientSecret"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGitLabIssues(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["accessToken", "projectPath"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGitLabLegislation(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["accessToken", "groupPath"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

export function validateServerConfig(data) {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors = {}
  const remainingKeys = new Set(Object.keys(data))

  {
    const key = "dataDir"
    if (remainingKeys.delete(key)) {
      let dir = data[key]
      if (typeof dir !== "string") {
        errors[key] = `Expected a string got "${typeof data}"`
      } else {
        dir = path.resolve(path.normalize(path.join("src/", dir.trim())))
        if (!fs.existsSync(dir)) {
          try {
            fs.mkdirSync(dir)
          } catch (e) {
            errors[key] = `Creation of directory "${dir}" failed: ${e}`
          }
        } else {
          if (!fs.lstatSync(dir).isDirectory()) {
            errors[key] = `Node "${dir}" isn't a directory`
          }
        }
      }
      data[key] = dir
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "gitlabAuthentication"
    remainingKeys.delete(key)
    const [value, error] = validateGitLabAuthentication(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "gitlabIssues"
    remainingKeys.delete(key)
    const [value, error] = validateGitLabIssues(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "gitlabLegislation"
    remainingKeys.delete(key)
    const [value, error] = validateGitLabLegislation(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let [key, dirName] of [
    ["sessionsDir", "sessions"],
    ["usersDir", "users"],
  ]) {
    remainingKeys.delete(key)
    let dir = data[key]
    if ((dir === null || dir === undefined) && !errors.dataDir) {
      dir = path.join(data.dataDir, dirName)
    } else if (typeof dir === "string") {
      dir = path.resolve(path.normalize(path.join("src/", dir.trim())))
    }
    if (typeof dir !== "string") {
      errors[key] = `Expected a string got "${typeof data}"`
    } else {
      if (!fs.existsSync(dir)) {
        try {
          fs.mkdirSync(dir)
        } catch (e) {
          errors[key] = `Creation of directory "${dir}" failed: ${e}`
        }
      } else {
        if (!fs.lstatSync(dir).isDirectory()) {
          errors[key] = `Node "${dir}" isn't a directory`
        }
      }
    }
    data[key] = dir
  }

  {
    const key = "sessionSecret"
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
