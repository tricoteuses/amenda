import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core"
import fetch from "cross-fetch"
import url from "url"

import serverConfig from "../../server_config"

const { gitlabLegislation: gitlab } = serverConfig

export async function post(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthorized: User is not authenticated.",
          },
        },
        null,
        2
      )
    )
  }

  const [body, error] = validateBody(req.body)
  if (error !== null) {
    console.error(
      `Error in form body:\n${JSON.stringify(body, null, 2)}\n\nError:\n${JSON.stringify(error, null, 2)}`
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...body,
          error: {
            code: 400,
            details: error,
            message: "Invalid form body",
          },
        },
        null,
        2
      )
    )
  }

  const groupSegments = body.path.split("/")
  let fileNodes = []
  // let fileUpdate = null
  let group = null
  const projectName = groupSegments.pop()
  const groupPath = `${gitlab.groupPath}/${groupSegments.join("/")}`
  const projectPath = `${groupPath}/${projectName}`
  let state = "get group"
  let userNamespace = null
  let userProject = null
  const userProjectPath = `${user.username}/${projectName}`
  while (state !== null) {
    switch (state) {
      case "get group":
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/groups/${encodeURIComponent(groupPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          group = await response.json()
        }
        state = "create project"
        break

      case "create project":
        // Try to create project.
        {
          const requestUrl = url.resolve(gitlab.url, "api/v4/projects")
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                // description:
                issues_enabled: false,
                jobs_enabled: false,
                merge_requests_enabled: false,
                // name:
                namespace_id: group.id,
                path: projectName,
                snippets_enabled: false,
                visibility: "public",
                wiki_enabled: false,
              }, null, 2),
              method: "POST",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            if (response.status === 400) {
              // Project already exists.
              state = "get project files"
              break
            }
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          // project = await response.json()
        }
        state = "upsert legislation file"
        break

      case "get project files":
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(projectPath)}/repository/tree`,
          )
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          const tree = await response.json()
          fileNodes = tree.filter(node => node.type === "blob")
        }
        state = "upsert legislation file"
        break

      case "upsert legislation file":
        {
          const markdownFilename = `${projectName}.md`
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(projectPath)}/repository/files/${
              encodeURIComponent(markdownFilename)}`,
          )
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                branch: "master",
                commit_message: "Ajout du fichier de législation à amender",
                content: body.legislation,
              }, null, 2),
              method: fileNodes.some(node => node.name === markdownFilename) ? "PUT" : "POST",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError: ${response.status} ${response.statusText}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          // fileUpdate = await response.json()
        }
        state = "upsert json file"
        break

      case "upsert json file":
        {
          const jsonFilename = `${projectName}.json`
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(projectPath)}/repository/files/${encodeURIComponent(jsonFilename)}`,
          )
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                branch: "master",
                commit_message: "Ajout du fichier JSON décrivant la législation à amender",
                content: JSON.stringify({
                  hostname: body.hostname,
                  originalHostname: body.originalHostname,
                  originalUrl: body.originalUrl,
                  title: body.title,
                  type: body.type,
                  url: body.url,
                }, null, 2),
              }, null, 2),
              method: fileNodes.some(node => node.name === jsonFilename) ? "PUT" : "POST",
              headers: {
                "Content-Type": "application/json; charset=utf-8",
                "Private-Token": gitlab.accessToken,
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError: ${response.status} ${response.statusText}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          // fileUpdate = await response.json()
        }
        state = "fork project"
        break

      case "fork project":
        // Get the namespace where to fork the project.
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/namespaces/${encodeURIComponent(user.username)}`,
          )
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userNamespace = await response.json()
        }

        // Try to fork project.
        {
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(projectPath)}/fork?namespace=${userNamespace.id}`,
          )
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({}, null, 2),
              method: "POST",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            if (response.status === 409) {
              // Conflict: path or name have already been taken.
              // Project already exists. Update it.
              state = "update user project"
              break
            }
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
        }
        state = "wait fork finished"
        break

      case "update user project":
        // TODO: Currently, the user project is only retrieved (and not updated).
        // const repositoryDir = fs.mkdtempSync(path.join(os.tmpdir(), "amenda_"))
        // console.log("repositoryDir", repositoryDir)
        // const cloneResult = childProcess.execSync(
        //   `/usr/bin/git clone https://archeo-lex.fr/codes/${body.code} ${repositoryDir}`)
        // console.log("cloneResult", cloneResult)
        // fs.removeSync(repositoryDir)
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/projects/${encodeURIComponent(userProjectPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
        }
        state = "update user legislation"
        break

      case "wait fork finished":
        {
          const requestUrl = url.resolve(gitlab.url, `api/v4/projects/${encodeURIComponent(userProjectPath)}`)
          const response = await fetch(
            requestUrl,
            {
              method: "GET",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError:\n${JSON.stringify(error, null, 2)}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          userProject = await response.json()
          const importStatus = userProject.import_status
          switch (importStatus) {
            case "finished":
            case "none":
              state = "update user legislation"
              break
            case "scheduled":
            case "started":
              await sleep(1000)
              // Continue with same state.
              break
            default:
              console.error(
                `Error while waiting for fork completion ${requestUrl}\n\nError: Unexpected import_status: ${
                  importStatus}`
              )
              res.writeHead(response.status, {
                "Content-Type": "application/json; charset=utf-8",
              })
              return res.end(
                JSON.stringify(
                  {
                    ...body,
                    error: {
                      code: 400,
                      message: `Unexpected import_status: ${importStatus}`,
                    },
                    requestUrl: requestUrl,
                    userProject,
                  },
                  null,
                  2
                )
              )
          }
        }
        break

      case "update user legislation":
        {
          const markdownFilename = `${projectName}.md`
          const requestUrl = url.resolve(
            gitlab.url,
            `api/v4/projects/${encodeURIComponent(userProjectPath)}/repository/files/${
              encodeURIComponent(markdownFilename)}`,
          )
          const response = await fetch(
            requestUrl,
            {
              body: JSON.stringify({
                branch: userProject.default_branch,
                commit_message: "Modification",
                content: body.legislationEdited,
              }, null, 2),
              method: "PUT",
              headers: {
                "Authorization": `Bearer ${user.accessToken}`,
                "Content-Type": "application/json; charset=utf-8",
              },
            }
          )
          if (!response.ok) {
            const responseBody = 400 <= response.status && response.status < 500
              ? await response.json()
              : await response.text()
            console.error(
              `Error while calling ${requestUrl}\n\nError: ${response.status} ${response.statusText}`
            )
            res.writeHead(response.status, {
              "Content-Type": "application/json; charset=utf-8",
            })
            return res.end(
              JSON.stringify(
                {
                  ...body,
                  error: {
                    body: responseBody,
                    code: response.status,
                    message: response.statusText,
                  },
                  requestUrl: requestUrl,
                },
                null,
                2
              )
            )
          }
          // fileUpdate = await response.json()
        }
        state = null
        break

      default:
        throw Error(`Unexpected state: ${state}`)
    }
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify({
    repositoryPath: userProjectPath,
    repositoryUrl: url.resolve(gitlab.url, userProjectPath),
    upstreamRepositoryPath: projectPath,
    upstreamRepositoryUrl: url.resolve(gitlab.url, projectPath),
  }, null, 2))
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function validateBody(body) {
  if (body === null || body === undefined) {
    return [body, "Missing body"]
  }
  if (typeof body !== "object") {
    return [body, `Expected an object, got ${typeof body}`]
  }

  body = {
    ...body,
  }
  const remainingKeys = new Set(Object.keys(body))
  const errors = {}

  for (let key of ["hostname", "legislation", "legislationEdited", "originalHostname", "path", "title", "type"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of ["originalUrl", "url"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(body[key])
    body[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [body, Object.keys(errors).length === 0 ? null : errors]
}
