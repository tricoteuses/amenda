import bodyParser from "body-parser"
import compression from "compression"
import session from "express-session"
import fs from "fs"
import passport from "passport"
import { Strategy as GitLabStrategy } from "passport-gitlab2"
import polka from "polka"
import makeSessionFileStore from "session-file-store"
import path from "path"
import sirv from "sirv"

import * as sapper from "@sapper/server"
import serverConfig from "./server_config"

import "material-icons/iconfont/material-icons.css"
import "typeface-roboto"
import "prosemirror-view/style/prosemirror.css"
import "prosemirror-gapcursor/style/gapcursor.css"
import "prosemirror-menu/style/menu.css"
import "prosemirror-example-setup/style/style.css"

import "./styles/index.css"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"
const SessionFileStore = makeSessionFileStore(session)
const { gitlabAuthentication, sessionsDir, sessionSecret, usersDir } = serverConfig

passport.deserializeUser(function(userId, done) {
  const userFilePath = path.join(usersDir, `user_${userId}.json`)
  if (!fs.existsSync(userFilePath)) {
    console.log(
      `passport.deserializeUser: User ${userId} not found (${userFilePath} doesn't exist)`,
    )
    return done(null, null)
  }
  if (!fs.lstatSync(userFilePath).isFile()) {
    console.log(
      `passport.deserializeUser: User ${userId} not found (${userFilePath} is not a file)`,
    )
    return done(null, null)
  }
  const userText = fs.readFileSync(userFilePath)
  done(null, JSON.parse(userText))
})

passport.serializeUser(function(user, done) {
  done(null, user.id)
})

passport.use(
  new GitLabStrategy(
    {
      baseURL: gitlabAuthentication.url,
      callbackURL: gitlabAuthentication.callbackUrl,
      clientID: gitlabAuthentication.clientId,
      clientSecret: gitlabAuthentication.clientSecret,
    },
    async function(accessToken, refreshToken, user, done) {
      user = {
        ...user,
        accessToken,
        refreshToken,
      }
      const userFilePath = path.join(usersDir, `user_${user.id}.json`)
      fs.writeFileSync(userFilePath, JSON.stringify(user, null, 2))
      return done(null, user)
    },
  ),
)

const app = polka()

app.set("trust proxy", dev ? false : 1) // In production, trust first proxy.

if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}
app
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    session({
      cookie: {
        // maxAge: 31536000,
        sameSite: "lax", // See https://scotthelme.co.uk/csrf-is-really-dead/
        secure: "auto",
      },
      resave: false,
      saveUninitialized: false,
      secret: sessionSecret,
      store: new SessionFileStore({
        path: sessionsDir,
      }),
      unset: "destroy",
    }),
    bodyParser.json({
      limit: "2mb", // Editing "Code civil" requires more than 1 Mb.
    }),
    bodyParser.urlencoded({
      extended: false,
      limit: "2mb", // Editing "Code civil" requires more than 1 Mb.
      // type: "application/x-www-form-urlencoded",
    }),
    passport.initialize(),
    passport.session(),
    sapper.middleware({
      session: (req /* , res */) => {
        const nouveau = req.session.nouveau || null
        delete req.session.nouveau
        return {
          nouveau,
          user: req.user,
        }
      },
    }),
  )
  .listen(PORT, error => {
    if (error) {
      console.log(`Error when calling listen on port ${PORT}:`, error)
    }
  })
