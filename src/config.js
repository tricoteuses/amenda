import { validateConfig } from "./validators/config"

const config = {
  globalAlert: {
    class: "is-danger",
    messageHtml: `
      <article class="container mx-auto my-4" role="alert">
        <div class="bg-orange font-bold rounded-t px-4 py-2 text-white">
          <i class="fas fa-exclamation-triangle"></i> Attention&nbsp;!
        </div>
        <div class="bg-orange-lightest border border-orange-light border-t-0 px-4 py-3 rounded-b text-orange-dark">
          <p>
            Ce site est <b>en développement</b>. Les informations qui y figurent peuvent être fausses ou
            incomplètes.
          </p>
          <p class="leading-normal">
            N'hésitez pas à nous
            <a href="https://framagit.org/tricoteuses/amenda/issues" target="_blank" title="Support technique d'Amenda">signaler des problèmes</a>
            où même à
            <a href="https://framagit.org/tricoteuses" target="_blank" title="Forge logicielle des Tricoteuses">
              tricoter des améliorations
            </a>.
          </p>
        </div>
      </article>
    `,
  },
  leftMenu: [
    {
      contentHtml: "Travaux parlementaires",
      title: "Projets et propositions de lois, rapports, résolutions, etc",
      url: "http://localhost:3000/",
    },
    {
      contentHtml: "Travaux citoyens",
      prefetch: true,
      title: "Propositions de lois, amendements",
      url: ".",
    },
    {
      contentHtml: "Parlementaires",
      title: "Députés et sénateurs",
      url: "http://localhost:3000/deputes",
    },
  ],
  missionStatement: "Connaître, comprendre et améliorer le travail parlementaire.",
  rightMenu: [
    {
      contentHtml: "À propos",
      title: "Informations sur ce site web",
      url: "http://localhost:3000/a-propos",
    },
  ],
  title: "Tricoteuses",
  url: "http://localhost:3000/",
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(validConfig, null, 2)}\nError:\n${JSON.stringify(error, null, 2)}`
  )
  process.exit(-1)
}

export default validConfig
