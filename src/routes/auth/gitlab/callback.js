import passport from "passport"

export async function get(req, res, next) {
  return passport.authenticate(
    "gitlab",
    function(err, user /* , info */) {
      if (err) {
        return next(err)
      }
      const url =
        req.baseUrl
        + (req.session.redirect || "/")
        + (req.session.action ? `?action=${encodeURIComponent(req.session.action)}` : "")
      delete req.session.action
      delete req.session.redirect
      if (!user) {
        // res.setHeader("Content-Type", "application/json")
        // return res.end(JSON.stringify({ error: info }, null, 2))
        res.writeHead(302, {
          "Location": url,
        })
        return res.end()
      }
      req.login(user, function(err) {
        if (err) {
          return next(err)
        }
        // res.setHeader("Content-Type", "application/json")
        // return res.end(JSON.stringify(user, null, 2))
        res.writeHead(302, {
          "Location": url,
        })
        return res.end()
      })
    }
  )(req, res, next)
}