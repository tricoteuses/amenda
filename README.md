# Amenda

## _Edit French law using Git_

_Amenda_ is free and open source software.

* [software repository](https://framagit.org/tricoteuses/amenda)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/tricoteuses/amenda/blob/master/LICENSE.md)

## Installation

### Install dependencies

```bash
npm install
```

## Server Configuration

Create a `.env` file to set configuration variables (you can use `example.env` as a template).

## Server Launch

In development mode:

```bash
npm run dev
```

In production mode:

```bash
npm run build
npm start
```
