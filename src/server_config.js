require("dotenv").config()

import { validateServerConfig } from "./validators/server_config"

const serverConfig = {
  dataDir: "../data/",
  gitlabAuthentication: {
    callbackUrl: "http://localhost:3001/auth/gitlab/callback",
    clientId: "CLIENT_ID", // Change it!
    clientSecret: "CLIENT_SECRET", // Change it!
    url: "https://framagit.org/",
  },
  gitlabIssues: {
    url: "https://framagit.org/",
    accessToken: "ACCESS_TOKEN", // Change it!
    projectPath: "tricoteuses/amenda",
  },
  gitlabLegislation: {
    url: "https://framagit.org/",
    accessToken: "ACCESS_TOKEN", // Change it!
    groupPath: "amenda",
  },
  sessionSecret: process.env.AMENDA_SESSION_SECRET,
}

const [validServerConfig, error] = validateServerConfig(serverConfig)
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      validServerConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validServerConfig
